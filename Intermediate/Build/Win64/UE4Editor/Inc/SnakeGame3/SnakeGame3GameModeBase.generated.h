// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME3_SnakeGame3GameModeBase_generated_h
#error "SnakeGame3GameModeBase.generated.h already included, missing '#pragma once' in SnakeGame3GameModeBase.h"
#endif
#define SNAKEGAME3_SnakeGame3GameModeBase_generated_h

#define SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_RPC_WRAPPERS
#define SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeGame3GameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeGame3GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeGame3GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/SnakeGame3"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGame3GameModeBase)


#define SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakeGame3GameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeGame3GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeGame3GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/SnakeGame3"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGame3GameModeBase)


#define SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGame3GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGame3GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGame3GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGame3GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGame3GameModeBase(ASnakeGame3GameModeBase&&); \
	NO_API ASnakeGame3GameModeBase(const ASnakeGame3GameModeBase&); \
public:


#define SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGame3GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGame3GameModeBase(ASnakeGame3GameModeBase&&); \
	NO_API ASnakeGame3GameModeBase(const ASnakeGame3GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGame3GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGame3GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGame3GameModeBase)


#define SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_12_PROLOG
#define SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_RPC_WRAPPERS \
	SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_INCLASS \
	SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME3_API UClass* StaticClass<class ASnakeGame3GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame3_Source_SnakeGame3_SnakeGame3GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
